package net.sweblog.gf4ex.jms.resourceinjection;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import java.util.logging.Logger;

/**
 *
 *
 */
@Singleton
public class DummyService
{
  public Logger logger = Logger.getLogger("JMS ResInJec");

  @PostConstruct
  public void sayHello()
  {
    logger.info("I am up!");
  }


  @PreDestroy
  public void sayGoodbye()
  {
    logger.info("I am down!");
  }

}
