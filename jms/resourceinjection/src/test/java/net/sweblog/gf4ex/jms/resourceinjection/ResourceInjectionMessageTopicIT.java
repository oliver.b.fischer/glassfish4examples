package net.sweblog.gf4ex.jms.resourceinjection;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.transaction.UserTransaction;
import java.util.UUID;

/**
 *
 *
 * @see <a href="https://java.net/jira/browse/GLASSFISH-15104">GLASSFISH-15104</a>
 * @see <a href="https://java.net/jira/browse/GLASSFISH-19711">GLASSFISH-19711</a>
 */
// @todo Finish this testcase
// @todo Add a note, that this testcase is written as for JMS 1.1
@Test(enabled = false)
public class ResourceInjectionMessageTopicIT
  extends Arquillian
{

  @Resource(mappedName = "myTopic")
  protected Topic topic;

  @Resource(mappedName = "jms/topicConnectionFactory")
  protected TopicConnectionFactory connectionFactory;

  @Deployment
  public static JavaArchive buildDeployment()
  {
    JavaArchive archive = ShrinkWrap.create(JavaArchive.class);

    archive.addClass(DummyService.class);

    return archive;
  }


  public void testCase() throws JMSException
  {

    Connection connection = null;
    Session session = null;
    MessageProducer producer = null;

    try {
      connection = connectionFactory.createConnection();
      connection.start();

      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      producer = session.createProducer(topic);

      // We will send a small text message saying 'Hello' in Japanese
      ObjectMessage objMessage = session.createObjectMessage();

      objMessage.setObject("Snafu");
      objMessage.setJMSMessageID(UUID.randomUUID().toString());

      // Here we are sending the message!
      producer.send(objMessage);

    } finally {
      try { if (null != producer) { producer.close();}  } catch (JMSException e) { }
      try { if (null != session) {session.close(); } } catch (JMSException e) { }
      try { if (null != connection) { connection.close(); } } catch (JMSException e) { }
    }

  }
}
