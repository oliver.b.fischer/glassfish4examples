package gf4ex.jaxrs20.jspview;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.glassfish.grizzly.http.util.Header;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.Test;

import java.io.File;

import static org.hamcrest.Matchers.equalTo;

@Test
@RunAsClient
public class CollectionIT
    extends Arquillian
{

    @Deployment
    public static WebArchive buildDeployment()
    {
        WebArchive archive = ShrinkWrap.create(WebArchive.class);

        archive.addAsWebInfResource(new File("src/main/webapp/WEB-INF/web.xml"),
                                    "web.xml")
               .addAsWebInfResource(
                   new File("src/main/webapp/WEB-INF/glassfish-web.xml"),
                   "glassfish-web.xml");

        archive.addClasses(Collection.class, MyApplication.class);

        archive.addAsWebResource(new File("src/main/webapp/index.jsp"),
                                 "index.jsp");

        return archive;
    }

    public void testGetToGetTextPlain()
    {
        RestAssured.given()
                   .log().all()
                   .header("Accept", "text/plain")

                   .expect()
                   .contentType(ContentType.TEXT)
                   .body(equalTo("hello"))
                   .statusCode(200)

                   .when()
                   .log().all()
                   .get("/jspview/rest881/collection");
    }

    @Test(enabled = false) // Does not work on the command line but in IDEA
    public void testGetToGetTextHtml()
    {
        RestAssured.given()
                   .log().all()
                   .header("Accept", "text/html")

                   .expect()
                   .contentType(ContentType.HTML)
                   .statusCode(200)

                   .when()
                   .log().all()
                   .get("/jspview/rest881/collection");
    }

}
