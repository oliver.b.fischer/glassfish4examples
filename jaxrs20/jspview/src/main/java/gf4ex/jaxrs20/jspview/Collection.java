package gf4ex.jaxrs20.jspview;


import org.glassfish.jersey.server.mvc.Viewable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.OK;

@Path("collection")
public class Collection
{
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getValue()
    {
        return "hello";
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getDoc()
    {
        Viewable v = new Viewable("/index.jsp", null);
        return Response.status(OK).entity(v).build();
    }
}
