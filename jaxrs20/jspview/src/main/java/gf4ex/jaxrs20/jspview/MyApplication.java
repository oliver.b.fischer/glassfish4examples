package gf4ex.jaxrs20.jspview;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/rest881")
public class MyApplication extends ResourceConfig
{
    public MyApplication() {
        register(Collection.class);
        register(JspMvcFeature.class);
    }
}