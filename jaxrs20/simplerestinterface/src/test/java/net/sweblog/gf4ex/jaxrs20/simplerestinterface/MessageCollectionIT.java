package net.sweblog.gf4ex.jaxrs20.simplerestinterface;

import com.jayway.restassured.http.ContentType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static com.jayway.restassured.RestAssured.given;

@Test
public class MessageCollectionIT
  extends Arquillian {

  @Deployment
  public static WebArchive buildDeployment()
  {
    WebArchive war = ShrinkWrap.create(WebArchive.class);

    war.addClasses(Constant304.class, MessageCollection.class)
       .addClasses(SimpleRESTApplication.class);

    war.addAsWebInfResource("web.xml")
       .addAsWebInfResource("glassfish-web.xml");

    System.out.println(war.toString(true));

    return war;
  }


  public void sendHalloToMessageCollection()
  {
    given()
      .contentType(ContentType.TEXT).body("Hallo")
      .expect().statusCode(Response.Status.CREATED.getStatusCode())
      .put(Constant304.MESSAGE_COLLECTION_FQP);
  }

}
