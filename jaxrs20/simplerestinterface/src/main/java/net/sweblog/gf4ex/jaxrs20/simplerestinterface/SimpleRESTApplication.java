package net.sweblog.gf4ex.jaxrs20.simplerestinterface;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(Constant304.APPLICATION_PATH)
public class SimpleRESTApplication
    extends Application {


}
