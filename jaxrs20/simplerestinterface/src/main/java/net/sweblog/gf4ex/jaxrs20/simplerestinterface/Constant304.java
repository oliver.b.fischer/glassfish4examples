package net.sweblog.gf4ex.jaxrs20.simplerestinterface;

public final class Constant304 {
    public static final String LOGGER_NAME = "SimpleRESTInterface";

  public static final String APPLICATION_PATH = "/simplerest";

  public static final String MESSAGE_COLLECTION_SUBPATH = "messages";

  public static final String MESSAGE_COLLECTION_FQP =
    APPLICATION_PATH + "/" + MESSAGE_COLLECTION_SUBPATH;


  private Constant304()
  {
  }
}
