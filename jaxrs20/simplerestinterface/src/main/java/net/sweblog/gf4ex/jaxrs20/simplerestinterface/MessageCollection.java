package net.sweblog.gf4ex.jaxrs20.simplerestinterface;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Path(Constant304.MESSAGE_COLLECTION_SUBPATH)
public class MessageCollection {

    private Logger logger = Logger.getLogger(Constant304.LOGGER_NAME);

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public Response handlePOST(String messageIn) {
        logger.log(Level.INFO, "Incoming message: \"{0}\"", messageIn);

        return Response.status(Response.Status.CREATED).entity("Re: " + messageIn).build();
    }
}
