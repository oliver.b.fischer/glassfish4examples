package net.sweblog.gf4ex.jaxrs20.mappingwithjackson;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Path(Constants.ADDRESS_COLLECTION_SUBPATH)
public class AddressCollection
{
  Logger logger = Logger.getLogger("JME");

  @PUT
  @Produces(MediaType.TEXT_PLAIN)
  public javax.ws.rs.core.Response saveAddress(AddressJTO address)
  {
    logger.log(Level.INFO, "Got address for " + address.getFullname());

    return Response.status(Response.Status.CREATED).entity("Danke!").build();
  }
}
