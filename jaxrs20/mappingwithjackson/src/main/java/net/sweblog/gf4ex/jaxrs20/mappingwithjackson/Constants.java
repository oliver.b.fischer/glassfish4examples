package net.sweblog.gf4ex.jaxrs20.mappingwithjackson;

/**
 *
 *
 */
public class Constants
{

  public final static String APPLICATION_SUBPATH = "/rest";

  public final static String ADDRESS_COLLECTION_SUBPATH = "addresses";

  public final static String ADDRESS_COLLECTION_FQP =
    APPLICATION_SUBPATH + "/"+ ADDRESS_COLLECTION_SUBPATH;
}
