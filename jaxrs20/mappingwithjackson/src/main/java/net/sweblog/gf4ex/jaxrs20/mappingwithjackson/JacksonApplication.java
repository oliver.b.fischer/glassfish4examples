package net.sweblog.gf4ex.jaxrs20.mappingwithjackson;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath(Constants.APPLICATION_SUBPATH)
public class JacksonApplication
  extends Application
{
  @Override
  public Set<Class<?>> getClasses()
  {
    return super.getClasses();
  }
}
