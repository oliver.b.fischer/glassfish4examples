package net.sweblog.gf4ex.jaxrs20.mappingwithjackson;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.json.spi.JsonProvider;

public class AddressJTO
{

  private String fullname;

  private String street;

  private String code;

  private String city;

  @JsonProperty("stadt")
  public String getCity()
  {
    return city;
  }

  public void setCity(String city)
  {
    this.city = city;
  }

  @JsonProperty("plz")
  public String getCode()
  {
    return code;
  }

  public void setCode(String code)
  {
    this.code = code;
  }

  @JsonProperty("name")
  public String getFullname()
  {
    return fullname;
  }

  public void setFullname(String fullname)
  {
    this.fullname = fullname;
  }

  @JsonProperty("strasse")
  public String getStreet()
  {
    return street;
  }

  public void setStreet(String street)
  {
    this.street = street;
  }
}
