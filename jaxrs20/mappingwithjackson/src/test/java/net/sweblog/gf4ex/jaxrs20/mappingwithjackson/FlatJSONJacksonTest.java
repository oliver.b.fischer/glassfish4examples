package net.sweblog.gf4ex.jaxrs20.mappingwithjackson;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.jayway.restassured.config.ObjectMapperConfig.objectMapperConfig;
import static com.jayway.restassured.http.ContentType.JSON;
import static com.jayway.restassured.mapper.ObjectMapperType.JACKSON_2;
import static javax.ws.rs.core.Response.Status.CREATED;
import static net.sweblog.gf4ex.jaxrs20.mappingwithjackson.Constants.ADDRESS_COLLECTION_FQP;

/**
 * This test case checks if it is possible to send and receive
 * a flat Jackson annotated Json Transfer Object aka JTO.
 */
@RunAsClient
@Test
public class FlatJSONJacksonTest
  extends Arquillian
{
  @BeforeClass
  public void configureRestAssured()
  {
    ObjectMapperConfig objectMapperConfig = objectMapperConfig().jackson2ObjectMapperFactory(
      new Jackson2ObjectMapperFactory() {
        @Override
        public com.fasterxml.jackson.databind.ObjectMapper create(Class c, String s) {
          return new MapperBuilder().build();
        }
      });

    RestAssured.config = RestAssuredConfig.config().objectMapperConfig(objectMapperConfig);
  }

  @Deployment
  public static WebArchive buildDeployment()
  {
    WebArchive archive = ShrinkWrap.create(WebArchive.class);

    archive.addPackage(Constants.class.getPackage());

    archive.addAsWebInfResource("glassfish-web.xml")
           .addAsWebInfResource("web.xml");

    return archive;

  }

  @Test
  public void sendAddressWithAllFieldsSet()
  {
    AddressJTO address = new AddressJTO();

    address.setCity("Berlin");
    address.setStreet("Berliner Allee 191");
    address.setFullname("Peter Pan");
    address.setCode("10437");

    RestAssured.given().log().all()
               .contentType(JSON)
               .body(address, JACKSON_2)

               .expect()
               .statusCode(CREATED.getStatusCode())
               .put(ADDRESS_COLLECTION_FQP);
  }
}
