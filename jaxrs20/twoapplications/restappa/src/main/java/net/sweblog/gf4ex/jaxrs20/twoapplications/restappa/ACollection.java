package net.sweblog.gf4ex.jaxrs20.twoapplications.restappa;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 *
 */
@Path("a")
public class ACollection
{
    @Context
    Application application;

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public Response handlePost(String data)
    {

        return Response.status(Response.Status.OK).entity("Application A")
                       .build();
    }
}
