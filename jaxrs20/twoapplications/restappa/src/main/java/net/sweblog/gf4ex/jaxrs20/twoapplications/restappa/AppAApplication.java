package net.sweblog.gf4ex.jaxrs20.twoapplications.restappa;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 *
 */
@ApplicationPath("/")
public class AppAApplication
    extends Application
{
    @Override
    public Set<Class<?>> getClasses()
    {
        return new HashSet<Class<?>>()
        {
            {
//                add(AppAApplication.class);
                add(ACollection.class);
            }
        };
    }
}
