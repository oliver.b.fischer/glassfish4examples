package net.sweblog.gf4ex.jaxrs20.twoapplications.restappb;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 *
 *
 */
@ApplicationPath("/")
public class AppBApplication
    extends Application
{
    @Override
    public Set<Class<?>> getClasses()
    {
        return new HashSet<Class<?>>()
        {
            {
                add(BCollection.class);
            }
        };
    }
}
